module Pages.MapView.Update exposing (update)

import Task


--

import Model exposing (..)
import Msg exposing (Msg(..), MapViewPageMsg(..))
import Ports exposing (initMap, setMapData)
import Router
import Utils.Geo as Geo
import Utils.GoogleMaps as GoogleMaps


update : MapViewPageMsg -> MapPageModel -> ( MapPageModel, Cmd Msg )
update msg pageModel =
    case msg of
        Ready records ->
            init pageModel records

        FeatureClicked featureId ->
            case String.split ":" featureId of
                [ _, id, _ ] ->
                    pageModel
                        ! [ Task.perform
                                identity
                                (Task.succeed (OnRouteChange (MapView { pageModel | focused = Just id })))
                          ]

                _ ->
                    pageModel ! []

        InitMapDone ->
            { pageModel
                | isMapLoading = False
                , filters = pageModel.filters
            }
                ! []


init : MapPageModel -> List ExecutionLog -> ( MapPageModel, Cmd Msg )
init pageModel data =
    let
        recordsToShow =
            applyFilters pageModel.filters data

        currentMapOptions =
            pageModel.mapOptions

        coordinates =
            case pageModel.focused of
                Just id ->
                    recordsToShow
                        |> List.filter (\record -> record.id == id)
                        |> List.head
                        |> Maybe.map (\record -> ([ record.origin.latLng ] ++ (record.path |> Maybe.withDefault [])))
                        |> Maybe.withDefault []

                Nothing ->
                    recordsToShow |> List.map (\record -> record.origin.latLng)

        { zoom, center } =
            coordinates
                |> Geo.rectForCoordinates
                |> Maybe.map Geo.rectToZoomAndCenter
                |> Maybe.withDefault { zoom = currentMapOptions.zoom, center = currentMapOptions.center }

        mapOptions =
            { currentMapOptions
                | zoom = zoom
                , center = center
            }
    in
        { pageModel
            | isMapLoading = True
            , records = data
            , mapOptions = mapOptions
        }
            ! [ GoogleMaps.encodeMapOptions mapOptions |> initMap
              , recordsToShow |> GoogleMaps.toGeoJson pageModel.focused |> setMapData
              ]
