module Pages.MapView.View exposing (view)

import Html exposing (Html, div, text, h4, a, img, span, button)
import Html.Attributes exposing (..)


--

import Model exposing (..)
import Styles exposing (styles)
import Msg exposing (Msg(..))
import Utils
import View.Organisms.ExecutionLogCard


view : String -> MapPageModel -> Html Msg
view gapiKey model =
    let
        findById id =
            model.records
                |> List.filter (\record -> record.id == id)
                |> List.head

        hasFocusedLocation record =
            maybeFocusedRecord
                |> Maybe.map (\focused -> focused.origin.latLng == record.origin.latLng)
                |> Maybe.withDefault False

        maybeFocusedRecord =
            model.focused |> Maybe.andThen findById

        overlapedRecords =
            model.records |> (applyFilters model.filters) |> List.filter hasFocusedLocation

        maybeMask =
            if model.isMapLoading then
                div [ class "ui active inverted dimmer" ] [ div [ class "ui text loader" ] [ text "LOADING" ] ] |> Just
            else
                Nothing
    in
        div []
            [ div [ style [ ( "position", "relative" ) ] ]
                ([ div [ id "map", class styles.mapContainer ] [ text "" ]
                 ]
                    ++ (maybeMask |> Utils.toList)
                )
            , div [ class "ui four column doubling stackable grid vertical segment" ]
                (overlapedRecords
                    |> List.map
                        (\record ->
                            View.Organisms.ExecutionLogCard.view
                                OnRouteChange
                                record
                                (MapView { model | focused = Just record.id, filters = model.filters })
                                (model.focused
                                    |> Maybe.map (\id -> id == record.id)
                                    |> Maybe.withDefault False
                                )
                                gapiKey
                        )
                )
            ]
