module Pages.NotFound.View exposing (view)

import Html exposing (Html, div, text, i, h2)
import Html.Attributes exposing (..)


view : Html msg
view =
    h2 [ class "ui center aligned grey icon header" ]
        [ i [ class "file outline icon" ] []
        , div [ class "content" ] [ text "Nothing here.", div [ class "sub header" ] [ text "Please go back..." ] ]
        ]
