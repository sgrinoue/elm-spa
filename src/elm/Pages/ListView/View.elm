module Pages.ListView.View exposing (view)

import Date.Extra.Config.Config_en_us exposing (config)
import Date.Extra.Core as DateExtra
import Date.Extra.Duration as Duration
import Date.Extra.Format as DateFormat
import Html exposing (Html, a, i, table, tbody, thead, tr, td, th, div, text)
import Html.Attributes exposing (..)


--

import Utils.Dom as Dom
import Model exposing (..)
import Msg exposing (Msg(..))
import Styles exposing (styles)
import View.Atoms.Marker
import View.Atoms.Status


view : Filters -> List ExecutionLog -> Html Msg
view appliedFilters records =
    let
        items =
            records
                |> List.sortBy (.startDateTime >> DateExtra.toTime >> negate)
                |> List.map (rowView appliedFilters)
    in
        div []
            [ table [ class "ui striped very basic table" ]
                [ thead []
                    [ tr []
                        [ th [ class "center aligned" ] [ text "Time" ]
                        , th [] []
                        , th [ class "center aligned" ] [ text "Status" ]
                        , th [ class "center aligned" ] [ text "Location" ]
                        , th [ class "center aligned" ] [ text "Within" ]
                        , th [ class "center aligned" ] [ text "Mode" ]
                        , th [ class "center aligned" ] [ text "Preference" ]
                        , th [ class "center aligned" ] [ text "Took" ]
                        , th [ class "center aligned" ] [ text "Device" ]
                        ]
                    ]
                , tbody [] items
                ]
            ]


rowView : Filters -> ExecutionLog -> Html Msg
rowView appliedFilters item =
    let
        deviceIcon =
            case item.deviceType of
                Just "phone" ->
                    "mobile alternate"

                Just "tablet" ->
                    "tablet alternate"

                _ ->
                    "desktop"

        tookTime =
            case item.completeDateTime of
                Just complete ->
                    Duration.diff complete item.startDateTime
                        |> (\d -> (d.minute |> toString) ++ ":" ++ (d.second |> toString |> String.padLeft 2 '0'))

                Nothing ->
                    "-"

        basicCell =
            [ "ui center aligned" => True, styles.singleLineColumn => True ]
    in
        tr []
            [ td [ classList basicCell ]
                [ item.startDateTime |> DateFormat.format config DateFormat.isoTimeFormat |> text
                ]
            , td [ classList basicCell ]
                [ Dom.a
                    OnRouteChange
                    (MapView { initialMapPageModel | focused = Just item.id, filters = appliedFilters })
                    []
                    [ View.Atoms.Marker.view item 0.6 ]
                ]
            , td [ classList basicCell ]
                [ status item |> View.Atoms.Status.view [] ]
            , td []
                [ a
                    [ item.origin.address |> (++) "https://maps.google.com/?q=" |> href
                    , target "_blank"
                    ]
                    [ item.origin.address |> text, i [ class "external link alternate icon" ] [] ]
                ]
            , td [ classList basicCell ]
                [ item.time |> toFloat |> (\d -> d / 60) |> toString |> (\s -> s ++ " min.") |> text ]
            , td [ classList basicCell ]
                [ case item.mode of
                    Walking ->
                        i [ class "large paw icon" ] []

                    Driving ->
                        i [ class "large car icon" ] []
                ]
            , td [ classList basicCell ]
                [ item.preference |> toString |> text
                ]
            , td [ classList basicCell ]
                [ tookTime |> text
                ]
            , td [ classList basicCell ]
                [ i
                    [ classList [ "large" => True, deviceIcon => True, "icon" => True ]
                    , title <| (item.userAgent |> Maybe.withDefault "(unknown)")
                    ]
                    []
                ]
            ]
