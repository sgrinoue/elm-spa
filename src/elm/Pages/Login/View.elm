module Pages.Login.View exposing (view)

import Html exposing (Html, i, img, div, label, p, a, input, form, text, textarea, button)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Decode


--

import Model exposing (Account, LoginPageModel)
import Msg exposing (LoginPageMsg(..), Msg(..))
import Styles exposing (styles)


view : LoginPageModel -> Html Msg
view model =
    div [ class "ui two column doubling middle aligned grid container" ]
        [ div [ class "computer only ten wide column" ]
            [ img [ class ([ "ui image", styles.loginPageImage ] |> String.join " "), src model.topImageUrl ] []
            ]
        , div [ class "six wide column ui raised segment" ]
            [ Html.form
                [ class "ui vertical segment form"
                , onWithOptions "submit" { stopPropagation = True, preventDefault = True } (Decode.succeed (NoOp))
                ]
                [ div [ class "field" ]
                    [ label [] [ text "Name" ]
                    , input
                        [ placeholder "name"
                        , value model.name
                        , onInput (LoginPageMsg << InputName)
                        ]
                        []
                    ]
                , div [ class "field" ]
                    [ label [] [ text "Password" ]
                    , input
                        [ type_ "password"
                        , value model.password
                        , onInput (LoginPageMsg << InputPassword)
                        ]
                        []
                    ]
                , div []
                    [ button
                        [ "ui teal fluid labeled icon button "
                            ++ (if model.isRequesting then
                                    "loading"
                                else
                                    ""
                               )
                            |> class
                        , onClick (LoginPageMsg ClickSignIn)
                        ]
                        [ text "Sign in", i [ class "sign in alternate icon" ] [] ]
                    , div
                        [ class "ui horizontal divider"
                        ]
                        [ text "OR" ]
                    , button
                        [ class "ui fluid labeled icon button primary"
                        , disabled True
                        ]
                        [ text "Create Account", i [ class "add icon" ] [] ]
                    , div [ class "ui center aligned basic segment" ] []
                    ]
                ]
            ]
        ]
