module Pages.Login.Update exposing (update)

import Task


--

import Model exposing (Account, LoginPageModel, Model, Route(..))
import Msg exposing (..)
import Router
import Source.Session as SessionSource


update : String -> LoginPageMsg -> LoginPageModel -> ( LoginPageModel, Cmd Msg )
update apiHost msg pageModel =
    case msg of
        InputName text ->
            { pageModel | name = text } ! []

        InputPassword text ->
            { pageModel | password = text } ! []

        ClickSignIn ->
            { pageModel | isRequesting = True }
                ! [ SessionSource.start
                        apiHost
                        ReceiveSignInResult
                        pageModel.name
                        pageModel.password
                        |> Cmd.map LoginPageMsg
                  ]

        ReceiveSignInResult (Err msg) ->
            ( { pageModel | isRequesting = False }, ErrorOccurred msg |> toCmd )

        ReceiveSignInResult (Ok account) ->
            ( { pageModel | isRequesting = False }, SessionMsg (SignInSuccess account) |> toCmd )

        Request (Just account) today ->
            ( pageModel, OnRouteChange (Router.top today) |> toCmd )

        Request Nothing _ ->
            ( pageModel, PageReady (Login pageModel) [] |> toCmd )


toCmd : Msg -> Cmd Msg
toCmd msg =
    Task.perform identity (Task.succeed msg)
