module Pages.Dashboard.View exposing (view)

import Date
import Dict
import Dict.Extra as DictExtra
import Html exposing (Html, div, text, table, tbody, tr, td, thead, th)
import Html.Attributes exposing (..)


--

import Model exposing (..)
import Styles exposing (styles)


view : List ExecutionLog -> Html msg
view records =
    div [] [ statsView records, heatMapView records ]


statsView : List ExecutionLog -> Html msg
statsView records =
    let
        executedCount =
            records |> List.length

        completedCount =
            records |> List.filter isCompleted |> List.length

        completeRatio =
            (toFloat completedCount / (toFloat executedCount)) * 100

        recordsByUser =
            records |> DictExtra.groupBy .clientIp

        userCount =
            recordsByUser
                |> Dict.size

        completedUserCount =
            recordsByUser
                |> Dict.filter (\_ records -> records |> List.filter isCompleted |> List.isEmpty |> not)
                |> Dict.size

        completeUserRatio =
            (toFloat completedUserCount / (toFloat userCount)) * 100
    in
        div [ class "ui very basic segment" ]
            [ div [ class "ui five statistics" ]
                [ div [ class "ui statistic" ]
                    [ div [ class "label" ] [ text "Executed" ]
                    , div [ class "value" ] [ executedCount |> toString |> text ]
                    ]
                , div [ class "ui statistic" ]
                    [ div [ class "label" ] [ text "Completed" ]
                    , div [ class "value" ] [ completedCount |> toString |> text ]
                    ]
                , div [ class "ui statistic" ]
                    [ div [ class "label" ] [ text "% Completion" ]
                    , div [ class "value" ] [ completeRatio |> toString |> String.slice 0 4 |> text ]
                    ]
                , div [ class "ui statistic" ]
                    [ div [ class "label" ] [ text "Users" ]
                    , div [ class "value" ] [ userCount |> toString |> text ]
                    ]
                , div [ class "ui statistic" ]
                    [ div [ class "label" ] [ text "% User w/ Completion" ]
                    , div [ class "value" ] [ completeUserRatio |> toString |> String.slice 0 4 |> text ]
                    ]
                ]
            ]


isCompleted : ExecutionLog -> Bool
isCompleted record =
    case record.completeDateTime of
        Just _ ->
            True

        Nothing ->
            False


heatMapView : List ExecutionLog -> Html msg
heatMapView records =
    let
        initialValues =
            List.range 0 5 |> List.map (\_ -> List.range 0 23 |> List.map (\_ -> 0))

        cells =
            records |> List.foldl transform initialValues

        max =
            cells |> List.map (List.maximum >> Maybe.withDefault 0) |> List.maximum |> Maybe.withDefault 0

        trs =
            cells |> List.indexedMap (,) |> List.map (heatMapRowView max)

        ths =
            tr [] ([ th [] [] ] ++ (List.range 0 23 |> List.map (\s -> s |> toString |> text |> List.singleton |> th [])))
    in
        table [ String.join " " [ "ui very basic segment unstackable table", styles.heatMap ] |> class ] [ tbody [] trs, thead [] [ ths ] ]


transform : ExecutionLog -> List (List Int) -> List (List Int)
transform record values =
    let
        hour =
            record.startDateTime |> Date.hour

        minute =
            record.startDateTime
                |> Date.minute
                |> toFloat
                |> (\a -> a / 10)
                |> floor
    in
        values
            |> List.indexedMap (,)
            |> List.map
                (\( i, row ) ->
                    row
                        |> List.indexedMap (,)
                        |> List.map
                            (\( j, cell ) ->
                                if i == minute && j == hour then
                                    cell + 1
                                else
                                    cell
                            )
                )


heatMapColumnView : Int -> Int -> Html msg
heatMapColumnView maxValue value =
    let
        opacity =
            if maxValue == 0 then
                0
            else
                (toFloat value) / (toFloat maxValue)
    in
        td
            []
            [ div
                [ style
                    [ ( "background-color", "#899c05" )
                    , ( "opacity", toString opacity )
                    ]
                ]
                []
            ]


heatMapRowView : Int -> ( Int, List Int ) -> Html msg
heatMapRowView maxValue ( rowIndex, values ) =
    tr []
        ([ td []
            [ rowIndex * 10 |> toString |> text ]
         ]
            ++ (values |> List.map (heatMapColumnView maxValue))
        )
