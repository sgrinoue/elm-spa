module Update exposing (init, update)

import Date exposing (Date)
import Dom.Scroll as Scroll
import Navigation exposing (Location)
import Task
import Tuple


--

import Http exposing (Error(..))
import Model exposing (..)
import Msg exposing (..)
import Pages.Login.Update
import Pages.MapView.Update
import Ports exposing (detectDeviceType)
import Router exposing (buildUrl)
import Source.ExecutionLog as ExecutionLogSource
import Source.Session as SessionSource
import Utils


init : Flags -> Location -> ( Model, Cmd Msg )
init flags location =
    { initialModel | isTransitioning = True, flags = flags }
        ! [ SessionSource.check flags.apiHost (SessionMsg << (SessionChecked location))
          , Task.perform Today Date.now
          ]


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        OnRouteChange route ->
            model ! [ route |> Router.buildUrl |> Navigation.newUrl ]

        OnLocationChange location ->
            Router.parseLocation location model.today
                |> requestRoute (model |> withTransitioning True)

        PageReady route data ->
            applyRoute model route data

        SessionMsg msg ->
            updateSession msg model

        UiEventMsg msg ->
            updateUi msg model

        Today date ->
            { model | today = date } ! []

        ErrorOccurred message ->
            (model
                |> withErrorNotification message
                |> withTransitioning False
            )
                ! []

        DetectDeviceTypeDone types ->
            (mapToRecords
                (\records ->
                    List.map2 (,) records types
                        |> List.map (\( record, deviceType ) -> { record | deviceType = deviceType })
                )
                model
            )
                ! []

        FetchSuccess toMsg fetchResult ->
            update (toMsg fetchResult.records)
                { model | cache = Just fetchResult }

        ChangeFilter filters ->
            Router.destructure model.route
                |> Maybe.map Tuple.first
                |> Maybe.map (\constructor -> OnRouteChange (constructor filters))
                |> Maybe.map (\route -> update route { model | isFilterViewShown = False })
                |> Maybe.withDefault (model ! [])

        _ ->
            case ( model.route, msg ) of
                ( Login pageModel, LoginPageMsg pageMsg ) ->
                    Pages.Login.Update.update model.flags.apiHost pageMsg pageModel |> toGlobal Login model

                ( MapView pageModel, ChildMsgMapView pageMsg ) ->
                    Pages.MapView.Update.update pageMsg pageModel |> toGlobal MapView model

                _ ->
                    let
                        _ =
                            Debug.log "unhandled" msg
                    in
                        model ! []


requestRoute : Model -> Route -> ( Model, Cmd Msg )
requestRoute model route =
    case ( model.account, route ) of
        ( _, NotFound (Just ( err, redirectTo )) ) ->
            update (OnRouteChange redirectTo) (model |> withErrorNotification err)

        ( _, Login pageModel ) ->
            Pages.Login.Update.update model.flags.apiHost (Request model.account model.today) pageModel |> toGlobal Login model

        ( Nothing, _ ) ->
            update (OnRouteChange (Login initialLoginPageModel)) model

        ( Just _, route ) ->
            Router.destructure route
                |> Maybe.map Tuple.second
                |> Maybe.map (\filters -> fetch model filters (PageReady route))
                |> Maybe.map (\cmd -> model ! [ cmd ])
                |> Maybe.withDefault (update (PageReady route []) model)


fetch : Model -> Filters -> (List ExecutionLog -> Msg) -> Cmd Msg
fetch model filters toMsg =
    model.cache
        |> Maybe.andThen
            (\cache ->
                if cache.filters.dateFrom == filters.dateFrom then
                    Just cache
                else
                    Nothing
            )
        |> Maybe.map (Task.succeed >> (Task.perform (FetchSuccess toMsg)))
        |> Maybe.withDefault
            (ExecutionLogSource.list
                model.flags.apiHost
                (Result.map (Cache filters >> FetchSuccess toMsg)
                    >> (Result.mapError remoteErrorToMsg)
                    >> Utils.unwrapResult
                )
                filters.dateFrom
            )


remoteErrorToMsg : Error -> Msg
remoteErrorToMsg error =
    case error of
        BadStatus response ->
            if response.status.code == 401 then
                SessionMsg SessionExpired
            else
                ErrorOccurred (toString error)

        _ ->
            ErrorOccurred (toString error)


applyRoute : Model -> Route -> List ExecutionLog -> ( Model, Cmd Msg )
applyRoute model route data =
    let
        ( updatedModel, cmd ) =
            case route of
                MapView pageModel ->
                    Pages.MapView.Update.update (Ready data) pageModel |> toGlobal MapView model

                ListView pageModel ->
                    ( { model | route = ListView { pageModel | records = data } }, Cmd.none )

                Dashboard pageModel ->
                    ( { model | route = Dashboard { pageModel | records = data } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )
    in
        { updatedModel | isTransitioning = False }
            ! [ cmd
              , Task.attempt (\_ -> NoOp) (Scroll.toTop "app")
              , data |> List.map (.userAgent >> Maybe.withDefault "") |> detectDeviceType
              ]


withInfoNotification : String -> Model -> Model
withInfoNotification value model =
    { model | notification = Just { level = Info, text = value } }


withErrorNotification : String -> Model -> Model
withErrorNotification value model =
    { model | notification = Just { level = Error, text = value } }


withoutNotification : Model -> Model
withoutNotification model =
    { model | notification = Nothing }


withTransitioning : Bool -> Model -> Model
withTransitioning value model =
    { model | isTransitioning = value }


withAccount : Account -> Model -> Model
withAccount value model =
    { model | account = Just value }


withoutAccount : Model -> Model
withoutAccount model =
    { model | account = Nothing }


withHeaderMenuShown : Bool -> Model -> Model
withHeaderMenuShown value model =
    { model | isHeaderMenuShown = value }


mapToRecords : (List ExecutionLog -> List ExecutionLog) -> Model -> Model
mapToRecords fn model =
    case model.route of
        Dashboard pageModel ->
            { model | route = Dashboard { pageModel | records = fn pageModel.records } }

        MapView pageModel ->
            { model | route = MapView { pageModel | records = fn pageModel.records } }

        ListView pageModel ->
            { model | route = ListView { pageModel | records = fn pageModel.records } }

        _ ->
            model


updateSession : SessionMsg -> Model -> ( Model, Cmd Msg )
updateSession msg model =
    case msg of
        RequestSignOut ->
            (model |> withTransitioning True)
                ! [ SessionSource.invalidate model.flags.apiHost (SessionMsg << SessionInvalidated) ]

        SessionInvalidated (Ok _) ->
            update (OnRouteChange (Login initialLoginPageModel))
                (model
                    |> withInfoNotification "You signed out. Bye!"
                    |> withTransitioning False
                    |> withoutAccount
                )

        SessionExpired ->
            update NoOp
                (model
                    |> withErrorNotification "Your session has been expired!"
                    |> withTransitioning False
                )

        SessionInvalidated (Err explanation) ->
            update (ErrorOccurred explanation) model

        SessionChecked location Nothing ->
            update (OnLocationChange location) (model |> withoutAccount)

        SessionChecked location (Just account) ->
            update (OnLocationChange location) (model |> withAccount account)

        SignInSuccess account ->
            update (OnRouteChange (Router.top model.today))
                (model
                    |> withAccount account
                    |> withInfoNotification ("Welcome, " ++ account.name)
                )


updateUi : UiEventMsg -> Model -> ( Model, Cmd Msg )
updateUi msg model =
    case msg of
        DismissNotification ->
            (model |> withoutNotification) ! []

        ToggleFilterView (Just a) ->
            { model | isFilterViewShown = a } ! []

        ToggleFilterView Nothing ->
            updateUi (ToggleFilterView (Just (not (model.isFilterViewShown)))) model

        ToggleHeaderMenu (Just value) ->
            (model |> withHeaderMenuShown value) ! []

        ToggleHeaderMenu Nothing ->
            updateUi (ToggleHeaderMenu (Just (not (model.isFilterViewShown)))) model

        ToggleMainMenu ->
            { model | isMenuShown = not (model.isMenuShown) } ! []


toGlobal : (a -> Route) -> Model -> ( a, Cmd Msg ) -> ( Model, Cmd Msg )
toGlobal routeConstructor model pageUpdate =
    let
        ( pageModel, cmd ) =
            pageUpdate
    in
        ( { model | route = routeConstructor pageModel }, cmd )
