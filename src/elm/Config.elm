module Config exposing (..)

import Date exposing (Date)


oldestDate : Date
oldestDate =
    Date.fromString "2016-09-04" |> Result.withDefault (Date.fromTime 0)
