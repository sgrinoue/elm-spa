module Router exposing (buildUrl, parseLocation, destructure, top)

import Date exposing (Date)
import Dict
import Navigation exposing (Location)
import UrlParser exposing ((</>), (<?>), s, int, string, stringParam)


--

import Model exposing (..)
import Utils
import Utils.Ymd as Ymd


type alias Param =
    Maybe String


top : Date -> Route
top today =
    let
        filters =
            { defaultFilters | dateFrom = today, dateTo = today }
    in
        MapView { initialMapPageModel | filters = filters }


parseLocation : Location -> Date -> Route
parseLocation location today =
    let
        filterTopParams path =
            path
                <?> stringParam "mode"
                <?> stringParam "status"
                <?> stringParam "preference"

        filterParams path =
            (filterTopParams path)
                <?> stringParam "dateFrom"
                <?> stringParam "dateTo"

        routes =
            UrlParser.oneOf
                [ UrlParser.map
                    (Login initialLoginPageModel)
                    (s "login")
                , UrlParser.map
                    (listParamsToRoute (\filters -> Dashboard { initialDashboardPageModel | filters = filters }))
                    (s "dashboard" |> filterParams)
                , UrlParser.map
                    (listParamsToRoute (\filters -> ListView { initialListPageModel | filters = filters }))
                    (s "execution-logs" |> filterParams)
                , UrlParser.map
                    (topParamsToRoute today)
                    (UrlParser.top |> filterTopParams)
                , UrlParser.map
                    (listParamsToRoute (\filters -> MapView { initialMapPageModel | filters = filters }))
                    (s "map" |> filterParams)
                , UrlParser.map
                    (detailParamsToRoute MapView)
                    (s "map" </> string |> filterParams)
                ]
    in
        location
            |> UrlParser.parsePath routes
            |> Debug.log "Route:"
            |> Maybe.withDefault (NotFound (Just ( "Unknown location: " ++ location.pathname, top today )))


topParamsToRoute : Date -> Param -> Param -> Param -> Route
topParamsToRoute today modeExpr statusExpr preferenceExpr =
    let
        todayExpr =
            Ymd.toYyyymmdd today |> Just
    in
        listParamsToRoute
            (\filters -> MapView { initialMapPageModel | filters = filters })
            modeExpr
            statusExpr
            preferenceExpr
            todayExpr
            todayExpr


listParamsToRoute : (Filters -> Route) -> Param -> Param -> Param -> Param -> Param -> Route
listParamsToRoute dateConsumer modeExpr statusExpr preferenceExpr dateFromExpr dateToExpr =
    parseFilters modeExpr statusExpr preferenceExpr dateFromExpr dateToExpr
        |> Result.map dateConsumer
        |> Result.mapError (\err -> NotFound (Just ( err, dateConsumer defaultFilters )))
        |> Utils.unwrapResult


detailParamsToRoute : (MapPageModel -> Route) -> String -> Param -> Param -> Param -> Param -> Param -> Route
detailParamsToRoute dateAndIdConsumer id modeExpr statusExpr preferenceExpr dateFromExpr dateToExpr =
    listParamsToRoute
        (\filters -> dateAndIdConsumer { initialMapPageModel | filters = filters, focused = Just id })
        modeExpr
        statusExpr
        preferenceExpr
        dateFromExpr
        dateToExpr


parseFilters : Param -> Param -> Param -> Param -> Param -> Result String Filters
parseFilters modeExpr statusExpr preferenceExpr dateFromExpr dateToExpr =
    Result.map5
        (\modes statuses preferences dateFrom dateTo ->
            { defaultFilters
                | status = statuses
                , mode = modes
                , preference = preferences
                , dateFrom = dateFrom
                , dateTo = dateTo
            }
        )
        (parseEnumericFilter "mode" allModes modeExpr)
        (parseEnumericFilter "status" allStatuses statusExpr)
        (parseEnumericFilter "preference" allPreferences preferenceExpr)
        (parseDateFilter "dateFrom" dateFromExpr)
        (parseDateFilter "dateTo" dateToExpr)


parseEnumericFilter : String -> List a -> Param -> Result String (List a)
parseEnumericFilter name allValues expr =
    let
        dict =
            allValues
                |> List.map (\v -> (toString v) => v)
                |> Dict.fromList
    in
        case expr of
            Nothing ->
                Ok allValues

            Just strs ->
                strs
                    |> String.split ","
                    |> List.map (\s -> Dict.get s dict |> Result.fromMaybe ("Unknown value for parameter '" ++ name ++ "': " ++ strs))
                    |> List.foldl (Result.map2 (\result passed -> [ result ] ++ passed)) (Ok [])


parseDateFilter : String -> Param -> Result String Date
parseDateFilter name expr =
    case expr of
        Just date ->
            Ymd.parseYyyymmdd date

        Nothing ->
            Err ("'" ++ name ++ "' is missing")


withFilters : Filters -> String -> String
withFilters filters path =
    let
        serializeDateFilter paramName value =
            value
                |> Ymd.toYyyymmdd
                |> (\v -> String.join "=" [ paramName, v ])
                |> List.singleton

        serializeEnumericFilter defaultValues paramName values =
            if values == defaultValues then
                []
            else
                values
                    |> List.map toString
                    |> String.join ","
                    |> (\v -> String.join "=" [ paramName, v ])
                    |> List.singleton

        serializeFilters filters =
            (serializeEnumericFilter defaultFilters.mode "mode" filters.mode)
                ++ (serializeEnumericFilter defaultFilters.status "status" filters.status)
                ++ (serializeEnumericFilter defaultFilters.preference "preference" filters.preference)
                ++ (serializeDateFilter "dateFrom" filters.dateFrom)
                ++ (serializeDateFilter "dateTo" filters.dateTo)
    in
        case serializeFilters filters of
            [] ->
                path

            params ->
                String.join "?" [ path, params |> String.join "&" ]


buildUrl : Route -> String
buildUrl route =
    case route of
        Login _ ->
            "/login"

        Dashboard pageModel ->
            "/dashboard" |> withFilters pageModel.filters

        ListView pageModel ->
            "/execution-logs" |> withFilters pageModel.filters

        MapView pageModel ->
            case pageModel.focused of
                Just id ->
                    ("/map/" ++ id) |> withFilters pageModel.filters

                Nothing ->
                    "/map" |> withFilters pageModel.filters

        NotFound _ ->
            "/not-found"


destructure : Route -> Maybe ( Filters -> Route, Filters )
destructure route =
    case route of
        Dashboard model ->
            Just ( \filters -> Dashboard { model | filters = filters }, model.filters )

        MapView model ->
            Just ( \filters -> MapView { model | filters = filters }, model.filters )

        ListView model ->
            Just ( \filters -> ListView { model | filters = filters }, model.filters )

        _ ->
            Nothing
