module Source.ExecutionLog exposing (list)

import Json.Decode as Decode exposing (Decoder, string, nullable, int, float, bool)
import Json.Decode.Pipeline as DecodePipeline exposing (required, optional, decode, hardcoded)
import Http exposing (Error)
import Date exposing (Date, Month(..))
import Date.Extra.Core as DateExtra


--

import Model exposing (..)
import Utils.Ymd as Ymd


decodeDate : Decoder Date
decodeDate =
    Decode.string
        |> Decode.andThen
            (\s ->
                case Date.fromString s of
                    Ok date ->
                        Decode.succeed date

                    Err e ->
                        Decode.fail e
            )


decodeMode : Decoder Mode
decodeMode =
    Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "WALKING" ->
                        Decode.succeed Walking

                    "DRIVING" ->
                        Decode.succeed Driving

                    value ->
                        --Decode.fail ("Unknown: " ++ value)
                        Decode.succeed Driving
            )


decodePreference : Decoder Preference
decodePreference =
    Decode.string
        |> Decode.andThen
            (\s ->
                case s of
                    "BALANCE" ->
                        Decode.succeed Balance

                    "PRECISION" ->
                        Decode.succeed Precision

                    "SPEED" ->
                        Decode.succeed Speed

                    value ->
                        Decode.fail ("Unknown: " ++ value)
            )


decodeResponse : Decode.Decoder ExecutionLog
decodeResponse =
    Decode.andThen
        (\origin ->
            decode ExecutionLog
                |> required "id" string
                |> required "startDateTime" decodeDate
                |> optional "completeDateTime" (nullable decodeDate) Nothing
                |> optional "path" (nullable (Decode.list decodeLatLng)) Nothing
                |> hardcoded origin
                |> required "mode" decodeMode
                |> required "time" int
                |> required "clientIp" string
                |> optional "preference" decodePreference Balance
                |> required "avoidFerries" bool
                |> required "avoidHighways" bool
                |> required "avoidTolls" bool
                |> required "anglePerStep" int
                |> required "url" string
                |> optional "userAgent" (nullable string) Nothing
                |> optional "viewport" (nullable decodeBrowserViewport) Nothing
                |> optional "encodedRequest" (nullable string) Nothing
                |> hardcoded Nothing
        )
        decodeOrigin


decodeBrowserViewport =
    Decode.map2 BrowserViewport
        (Decode.int |> Decode.field "width")
        (Decode.int |> Decode.field "height")


decodeLatLng =
    Decode.map2 LatLng
        (Decode.float |> Decode.field "latitude")
        (Decode.float |> Decode.field "longitude")


decodeOrigin : Decoder Origin
decodeOrigin =
    decode Origin
        |> required "originAddress" string
        |> required "originCoordinate" decodeLatLng


decodeListResponse : Decode.Decoder (List ExecutionLog)
decodeListResponse =
    decodeResponse |> Decode.list


list : String -> (Result Error (List ExecutionLog) -> msg) -> Date -> Cmd msg
list apiHost toMsg date =
    let
        year =
            date |> Date.year |> toString

        month =
            date |> Date.month |> DateExtra.monthToInt |> toString

        day =
            date |> Date.day |> toString
    in
        Http.send
            toMsg
            (Http.request
                { method = "GET"
                , url = apiHost ++ "/executionLogs?year=" ++ year ++ "&month=" ++ month ++ "&day=" ++ day
                , timeout = Nothing
                , headers = []
                , body = Http.emptyBody
                , withCredentials = True
                , expect = Http.expectJson decodeListResponse
                }
            )
