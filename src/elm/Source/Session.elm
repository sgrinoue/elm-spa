module Source.Session exposing (start, check, invalidate)

import Http
import Json.Decode as Decode
import Json.Encode as Encode


--

import Model exposing (..)


accountDecoder : Decode.Decoder Account
accountDecoder =
    Decode.string
        |> Decode.at [ "user", "username" ]
        |> Decode.map
            (\s ->
                { name = s
                , avatarImageUrl = "https://avatars2.githubusercontent.com/u/7351859?s=400&u=dca68c1308a6247018da93733a2de5af41e53913&v=4"
                }
            )


invalidate : String -> (Result String () -> msg) -> Cmd msg
invalidate apiHost toMsg =
    Http.send
        (Result.mapError toString >> toMsg)
        (Http.request
            { method = "DELETE"
            , url = apiHost ++ "/users/signin"
            , timeout = Nothing
            , headers = []
            , body = Http.emptyBody
            , withCredentials = True
            , expect = Http.expectStringResponse (\_ -> Ok ())
            }
        )


check : String -> (Maybe Account -> msg) -> Cmd msg
check apiHost toMsg =
    Http.send
        (Result.toMaybe >> toMsg)
        (Http.request
            { method = "GET"
            , url = apiHost ++ "/users/self"
            , timeout = Nothing
            , headers = []
            , body = Http.emptyBody
            , withCredentials = True
            , expect = Http.expectJson accountDecoder
            }
        )


start : String -> (Result String Account -> msg) -> String -> String -> Cmd msg
start apiHost toMsg name password =
    Http.send
        (Result.mapError toString >> toMsg)
        (Http.request
            { method = "POST"
            , url = apiHost ++ "/users/signin"
            , timeout = Nothing
            , headers = []
            , body =
                Encode.object
                    [ ( "email", Encode.string name )
                    , ( "password", Encode.string password )
                    ]
                    |> Http.jsonBody
            , withCredentials = True
            , expect = Http.expectJson accountDecoder
            }
        )
