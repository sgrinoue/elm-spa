module Msg exposing (..)

import Date exposing (Date)
import Navigation exposing (Location)


--

import Model exposing (..)


type Msg
    = ChangeFilter Filters
    | ChildMsgMapView MapViewPageMsg
    | DetectDeviceTypeDone (List (Maybe String))
    | ErrorOccurred String
    | FetchSuccess (List ExecutionLog -> Msg) Cache
    | LoginPageMsg LoginPageMsg
    | NoOp
    | OnLocationChange Navigation.Location
    | OnRouteChange Route
    | PageReady Route (List ExecutionLog)
    | SessionMsg SessionMsg
    | Today Date
    | UiEventMsg UiEventMsg


type UiEventMsg
    = DismissNotification
    | ToggleFilterView (Maybe Bool)
    | ToggleHeaderMenu (Maybe Bool)
    | ToggleMainMenu


type SessionMsg
    = RequestSignOut
    | SessionChecked Navigation.Location (Maybe Account)
    | SessionExpired
    | SessionInvalidated (Result String ())
    | SignInSuccess Account


type LoginPageMsg
    = ClickSignIn
    | InputName String
    | InputPassword String
    | ReceiveSignInResult (Result String Account)
    | Request (Maybe Account) Date


type MapViewPageMsg
    = FeatureClicked Id
    | Ready (List ExecutionLog)
    | InitMapDone
