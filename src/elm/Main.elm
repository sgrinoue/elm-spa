module Main exposing (..)

import Navigation


--

import Model exposing (Model, Flags)
import Msg exposing (Msg(..), MapViewPageMsg(..))
import Ports exposing (initMapDone, featureClicked, detectDeviceTypeDone)
import Update exposing (init, update)
import View.App exposing (view)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ initMapDone (\_ -> ChildMsgMapView InitMapDone)
        , featureClicked (ChildMsgMapView << FeatureClicked)
        , detectDeviceTypeDone DetectDeviceTypeDone
        ]


main : Program Flags Model Msg
main =
    Navigation.programWithFlags OnLocationChange
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
