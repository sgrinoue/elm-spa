module Utils.GoogleMaps exposing (toGeoJson, encodeMapOptions, markerColor, panoramaUrl)

import Json.Encode as Encode exposing (Value)


--

import Model exposing (..)
import Utils


toGeoJson : Maybe Id -> List ExecutionLog -> List Value
toGeoJson maybeFocused records =
    let
        isFocused record =
            case maybeFocused of
                Nothing ->
                    False

                Just id ->
                    id == record.id

        origins =
            records |> List.map (\record -> feature record (isFocused record))

        results =
            records |> List.filterMap (\record -> maybeResultFeature record (isFocused record))
    in
        origins ++ results


maybeResultFeature : ExecutionLog -> Bool -> Maybe Value
maybeResultFeature record isFocused =
    record.path
        |> Maybe.andThen
            (\path ->
                case path of
                    head :: _ ->
                        Just (path ++ [ head ])

                    _ ->
                        Nothing
            )
        |> Maybe.map
            (\closedPath ->
                Encode.object
                    [ "type" => Encode.string "Feature"
                    , "id" => Encode.string ("result:" ++ record.id ++ ":" ++ toString isFocused)
                    , "geometry" => resultGeometry closedPath
                    , "properties"
                        => Encode.object
                            [ "style" => resultGeometryStyle record isFocused ]
                    ]
            )


resultGeometryStyle : ExecutionLog -> Bool -> Encode.Value
resultGeometryStyle record isFocused =
    let
        ( fillOpacity, strokeOpacity, strokeWeight ) =
            if isFocused then
                ( 0.2, 1, 3 )
            else
                ( 0, 0, 0 )

        zIndex =
            case record.mode of
                Driving ->
                    100000 - record.time

                Walking ->
                    100000 - floor (toFloat record.time / 100.0)
    in
        Encode.object
            [ "fillOpacity" => Encode.float fillOpacity
            , "strokeOpacity" => Encode.float strokeOpacity
            , "strokeWeight" => Encode.int strokeWeight
            , "zIndex" => Encode.int zIndex
            ]


coordToGeoJson : LatLng -> Encode.Value
coordToGeoJson coord =
    [ Encode.float coord.lng, Encode.float coord.lat ] |> Encode.list


resultGeometry : List LatLng -> Value
resultGeometry coords =
    Encode.object
        [ "type" => Encode.string "Polygon"
        , "coordinates"
            => (coords
                    |> List.map coordToGeoJson
                    |> Encode.list
                    |> List.singleton
                    |> Encode.list
               )
        ]


feature : ExecutionLog -> Bool -> Value
feature record isFocused =
    Encode.object
        [ "type" => Encode.string "Feature"
        , "id" => Encode.string ("origin:" ++ record.id ++ ":" ++ toString isFocused)
        , "geometry" => startLocationGeometry record
        , "properties" => properties record isFocused
        ]


startLocationGeometry : ExecutionLog -> Value
startLocationGeometry record =
    Encode.object
        [ "type" => Encode.string "Point"
        , "coordinates" => coordToGeoJson record.origin.latLng
        ]


properties : ExecutionLog -> Bool -> Value
properties record isFocused =
    Encode.object
        [ "address" => Encode.string record.origin.address
        , "style" => markerStyle record isFocused
        ]


markerStyle : ExecutionLog -> Bool -> Encode.Value
markerStyle record isFocused =
    let
        recordMarkerColor =
            markerColor record

        ( strokeWeight, scale, zIndex ) =
            if isFocused then
                ( 2, 1, 2 )
            else
                ( 1, 0.6, 1 )
    in
        Encode.object
            [ "icon"
                => Encode.object
                    [ "fillColor" => (recordMarkerColor 0.6 |> Encode.string)
                    , "fillOpacity" => Encode.int 1
                    , "strokeColor" => (recordMarkerColor 0.4 |> Encode.string)
                    , "strokeWeight" => Encode.int strokeWeight
                    , "path" => Encode.string "M0-48c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z"
                    , "scale" => Encode.float scale
                    ]
            , "zIndex" => Encode.int zIndex
            ]


markerColor : ExecutionLog -> Float -> String
markerColor record =
    let
        hue =
            Utils.hash (record.clientIp) % 360 |> toString
    in
        \light -> "hsl(" ++ hue ++ ", 60%, " ++ (round (light * 100) |> toString) ++ "%)"


encodeMapOptions : MapOptions -> Encode.Value
encodeMapOptions mapOptions =
    Encode.object
        [ "center" => encodeLatLng mapOptions.center
        , "zoom" => Encode.int mapOptions.zoom
        , "gestureHandling" => Encode.string mapOptions.gestureHandling
        ]


encodeLatLng : LatLng -> Value
encodeLatLng coord =
    Encode.object
        [ "lat" => Encode.float coord.lat
        , "lng" => Encode.float coord.lng
        ]


panoramaUrl : String -> String -> String
panoramaUrl gapiKey location =
    "https://maps.googleapis.com/maps/api/streetview?size=400x150&fov=120&location=" ++ location ++ "&key=" ++ gapiKey
