module Utils.Geo exposing (rectForCoordinates, rectToZoomAndCenter)


rectForCoordinates coords =
    let
        epsilon =
            0.001
    in
        List.foldl
            (\{ lat, lng } passed ->
                case passed of
                    Nothing ->
                        Just
                            { sw = { lat = lat - epsilon, lng = lng - epsilon }
                            , ne = { lat = lat + epsilon, lng = lng + epsilon }
                            }

                    Just { sw, ne } ->
                        Just
                            { sw = { lat = min lat sw.lat, lng = min lng sw.lng }
                            , ne = { lat = max lat ne.lat, lng = max lng ne.lng }
                            }
            )
            Nothing
            coords


rectToZoomAndCenter rect =
    { center =
        { lat = (rect.sw.lat + rect.ne.lat) / 2
        , lng = (rect.sw.lng + rect.ne.lng) / 2
        }
    , zoom = max 2 (round (logBase 2 (360 / (rect.ne.lng - rect.sw.lng))))
    }
