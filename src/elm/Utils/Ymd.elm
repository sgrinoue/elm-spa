module Utils.Ymd
    exposing
        ( fromYmdToDate
        , toYmd
        , toYmdString
        , toYyyymmdd
        , parseYyyymmdd
        )

import Date exposing (Date, Month(..))
import Date.Extra.Core as DateExtra


fromYmdToDate : Int -> Int -> Int -> Result String Date
fromYmdToDate y m d =
    Date.fromString (String.join "-" [ y |> toString, m |> toString, d |> toString ])


toYmd : Date -> ( Int, Int, Int )
toYmd date =
    ( date |> Date.year
    , date |> Date.month |> DateExtra.monthToInt
    , date |> Date.day
    )


toYmdString : String -> Date -> String
toYmdString concatenator date =
    let
        ( y, m, d ) =
            toYmd date
    in
        String.join concatenator [ y |> toString, m |> toString, d |> toString ]


toYyyymmdd : Date -> String
toYyyymmdd date =
    let
        ( y, m, d ) =
            toYmd date
    in
        (y |> toString |> String.padLeft 4 '0')
            ++ (m |> toString |> String.padLeft 2 '0')
            ++ (d |> toString |> String.padLeft 2 '0')


parseYyyymmdd : String -> Result String Date
parseYyyymmdd expr =
    let
        y =
            expr |> String.slice 0 4

        m =
            expr |> String.slice 4 6

        d =
            expr |> String.slice 6 8
    in
        [ y, m, d ] |> String.join "-" |> Date.fromString
