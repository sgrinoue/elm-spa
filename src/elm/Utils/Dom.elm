module Utils.Dom exposing (a)

import Html exposing (Html)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Json.Decode as Decode


--

import Model exposing (..)
import Router


a : (Route -> msg) -> Route -> List (Html.Attribute msg) -> List (Html msg) -> Html msg
a toMsg route attributes children =
    Html.a
        (attributes
            ++ [ route |> Router.buildUrl |> href
               , onWithOptions
                    "click"
                    { stopPropagation = True, preventDefault = True }
                    (Decode.map (\_ -> toMsg route) Decode.value)
               ]
        )
        children
