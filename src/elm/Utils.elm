module Utils
    exposing
        ( toList
        , hash
        , unwrapResult
        )

import Bitwise
import Char


toList : Maybe a -> List a
toList maybe =
    maybe |> Maybe.map List.singleton |> Maybe.withDefault []


hash : String -> Int
hash str =
    let
        updateHash c h =
            (Bitwise.shiftLeftBy h 5) + h + Char.toCode c
    in
        String.foldl updateHash 5381 str


unwrapResult : Result a a -> a
unwrapResult result =
    case result of
        Ok value ->
            value

        Err value ->
            value
