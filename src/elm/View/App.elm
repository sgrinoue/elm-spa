module View.App exposing (view)

import Html exposing (Html, a, div, p, text, i, h2, h3, img, h1, button, label, input)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Html.Lazy exposing (lazy, lazy2)


--

import Model exposing (..)
import Msg exposing (..)
import Pages.Dashboard.View
import Pages.ListView.View
import Pages.Login.View
import Pages.MapView.View
import Pages.NotFound.View
import Router
import Styles exposing (styles)
import Utils
import Utils.Dom as Dom
import View.Molecules.Mask
import View.Organisms.Account
import View.Organisms.Filters
import View.Organisms.Menu


view : Model -> Html Msg
view model =
    let
        maybeMaskView =
            if model.isTransitioning then
                Just View.Molecules.Mask.view
            else
                Nothing
    in
        div [ class "ui grid container" ]
            ([ headerView model
             , div [ class "sixteen wide column" ] [ lazy contentView model ]
             ]
                ++ (maybeMaskView |> Utils.toList)
            )


headerView : Model -> Html Msg
headerView model =
    div [ class "sixteen wide column" ]
        ((model.account
            |> Maybe.map (lazy2 View.Organisms.Account.view model.isHeaderMenuShown)
            |> Maybe.map List.singleton
            |> Maybe.withDefault []
         )
            ++ [ h1 [ class ("ui medium center aligned header " ++ styles.title) ]
                    [ Dom.a
                        OnRouteChange
                        (Router.top model.today)
                        [ class "content" ]
                        [ text "walk30m dashboard" ]
                    ]
               ]
        )


notificationView : Model -> Html Msg
notificationView model =
    case model.notification of
        Just message ->
            let
                statusClass =
                    if message.level == Info then
                        "info"
                    else
                        "error"
            in
                div [ class "ui sixteen wide column" ]
                    [ p [ "ui " ++ statusClass ++ " message" |> class ]
                        [ text message.text
                        , i [ onClick (UiEventMsg DismissNotification), class "close icon" ] []
                        ]
                    ]

        Nothing ->
            div [ String.join " " [ "ui sixteen wide column", styles.hidden ] |> class ] []


contentView : Model -> Html Msg
contentView model =
    let
        withMenu filters content =
            div []
                [ lazy View.Organisms.Menu.view model
                , View.Organisms.Filters.view filters model.isFilterViewShown
                , content
                ]
    in
        case model.route of
            Login pageModel ->
                lazy Pages.Login.View.view pageModel

            MapView pageModel ->
                Pages.MapView.View.view model.flags.gapiKey pageModel |> withMenu pageModel.filters

            ListView pageModel ->
                lazy2 Pages.ListView.View.view pageModel.filters (applyFilters pageModel.filters pageModel.records) |> withMenu pageModel.filters

            Dashboard pageModel ->
                lazy Pages.Dashboard.View.view (applyFilters pageModel.filters pageModel.records) |> withMenu pageModel.filters

            _ ->
                Pages.NotFound.View.view |> withMenu defaultFilters
