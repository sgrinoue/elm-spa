module View.Organisms.Account exposing (view)

import Html exposing (Html, div, text, i, img)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


--

import Model exposing (..)
import Msg exposing (..)
import Styles exposing (styles)


view : Bool -> Account -> Html Msg
view isMenuShown account =
    let
        menuDisplay =
            if isMenuShown then
                "block"
            else
                "none"
    in
        div [ class styles.signedInStatusView ]
            [ div [ class "ui dropdown", onMouseOver (UiEventMsg (ToggleHeaderMenu (Just True))) ]
                [ div [ class "text" ] [ text account.name ]
                , i [ class "dropdown icon" ] []
                , div [ class "menu", style [ ( "display", menuDisplay ) ], onMouseOut (UiEventMsg (ToggleHeaderMenu (Just False))) ]
                    [ div [ class "item", onClick (SessionMsg RequestSignOut) ] [ text "Sign Out" ] ]
                ]
            ]
