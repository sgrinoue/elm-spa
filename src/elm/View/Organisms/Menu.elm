module View.Organisms.Menu exposing (view)

import Date exposing (Date)
import Date.Extra.Core as DateExtra
import Date.Extra.Period as DatePeriod
import Html exposing (Html, option, a, div, i, text, select, h2, button)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


--

import Config
import Model exposing (..)
import Msg exposing (..)
import Router
import Utils.Ymd as Ymd
import Utils.Dom as Dom


type alias Ymd =
    ( Int, Int, Int )


view : Model -> Html Msg
view model =
    let
        clz menuRoute =
            if isMatched menuRoute model.route then
                class "active item"
            else
                class "item"

        ( f, dateSelect ) =
            case ( model.route, Router.destructure model.route ) of
                ( MapView pageModel, Just ( dateConsumer, filters ) ) ->
                    ( filters
                    , [ dateSelectorView
                            (\f -> MapView { pageModel | focused = Nothing, filters = f })
                            model.today
                            filters
                      ]
                    )

                ( _, Just ( dateConsumer, filters ) ) ->
                    ( filters, [ dateSelectorView dateConsumer model.today filters ] )

                ( _, Nothing ) ->
                    ( defaultFilters, [] )

        ymd =
            f.dateFrom |> Ymd.toYyyymmdd

        gaUrl =
            "https://analytics.google.com/analytics/web/#report/visitors-overview/" ++ model.flags.googleAnalyticsId ++ "/%3F_u.date00%3D" ++ ymd ++ "%26_u.date01%3D" ++ ymd ++ "/"

        mapRoute =
            MapView { initialMapPageModel | filters = f }

        dashboardRoute =
            Dashboard { initialDashboardPageModel | filters = f }

        listRoute =
            ListView { initialListPageModel | filters = f }

        menuItems =
            if model.isMenuShown then
                [ Dom.a OnRouteChange dashboardRoute [ clz dashboardRoute ] [ text "Dashboard" ]
                , Dom.a OnRouteChange mapRoute [ clz mapRoute ] [ text "Map View" ]
                , Dom.a OnRouteChange listRoute [ clz listRoute ] [ text "List View" ]
                , a [ class "item", target "_blank", href gaUrl ] [ i [ class "external link alternate icon" ] [], text "Google Analytics" ]
                ]
                    ++ dateSelect
            else
                []

        hamburger =
            [ i [ class "ui mobile only grid large bars icon", onClick (UiEventMsg ToggleMainMenu) ] [] ]
    in
        div [ class "ui stackable secondary pointing big menu" ] (hamburger ++ menuItems)


isMatched : Route -> Route -> Bool
isMatched route1 route2 =
    case ( route1, route2 ) of
        ( Dashboard _, Dashboard _ ) ->
            True

        ( ListView _, ListView _ ) ->
            True

        ( MapView _, MapView _ ) ->
            True

        _ ->
            route1 == route2


dateSelectorView : (Filters -> Route) -> Date -> Filters -> Html Msg
dateSelectorView routeConstructor today filters =
    let
        date =
            filters.dateFrom

        dateConsumer d =
            routeConstructor { filters | dateFrom = d, dateTo = d }

        todayRoute =
            today |> dateConsumer

        isToday =
            Ymd.toYmd today == Ymd.toYmd filters.dateFrom

        nextDateRoute =
            DatePeriod.add DatePeriod.Day 1 date |> dateConsumer

        prevDateRoute =
            DatePeriod.add DatePeriod.Day -1 date |> dateConsumer

        filterColor =
            if isFiltered filters then
                ""
            else
                "grey"
    in
        div [ class "right menu" ]
            [ Dom.a OnRouteChange prevDateRoute [ class "item ui button" ] [ i [ class "caret left icon" ] [], text "Prev" ]
            , select [ class "item ui dropdown", onInput (handleDayChange filters) ]
                (List.map
                    (\d ->
                        option
                            [ toString d |> value
                            , (d == (date |> Date.day)) |> selected
                            ]
                            [ toString d |> text ]
                    )
                    (List.range 1 (DateExtra.daysInMonthDate date))
                )
            , select [ class "item ui dropdown", onInput (handleMonthChange filters) ]
                (List.map
                    (\m ->
                        option
                            [ DateExtra.monthToInt m |> toString |> value
                            , (m == (date |> Date.month)) |> selected
                            ]
                            [ m |> toString |> text ]
                    )
                    DateExtra.monthList
                )
            , select [ class "item ui dropdown", onInput (handleYearChange filters) ]
                (List.map
                    (\y ->
                        option
                            [ toString y |> value
                            , (y == (date |> Date.year)) |> selected
                            ]
                            [ toString y |> text ]
                    )
                    (today |> Date.year |> List.range (Config.oldestDate |> Date.year))
                )
            , Dom.a OnRouteChange
                todayRoute
                [ classList [ "item ui button" => True, "active" => isToday ]
                ]
                [ text "Today" ]
            , Dom.a OnRouteChange
                nextDateRoute
                [ classList [ "item ui button" => True, "disabled" => isToday ]
                ]
                [ text "Next", i [ class "caret right icon" ] [] ]
            , a
                [ class "item ui button"
                , onClick (UiEventMsg (ToggleFilterView Nothing))
                ]
                [ i [ class (filterColor ++ " filter icon") ] [] ]
            ]


handleDateChange : (Ymd -> Result a Ymd) -> Filters -> Msg
handleDateChange transformYmd filters =
    let
        ( y, m, d ) =
            Ymd.toYmd filters.dateFrom
    in
        case transformYmd ( y, m, d ) of
            Ok ( y, m, d ) ->
                Ymd.fromYmdToDate y m d
                    |> Result.map (\date -> ChangeFilter { filters | dateFrom = date, dateTo = date })
                    |> Result.withDefault NoOp

            Err _ ->
                NoOp


handleYearChange : Filters -> String -> Msg
handleYearChange filters yearTo =
    handleDateChange
        (\( _, m, d ) -> yearTo |> String.toInt |> Result.map (\y -> ( y, m, d )))
        filters


handleMonthChange : Filters -> String -> Msg
handleMonthChange filters monthTo =
    handleDateChange
        (\( y, _, d ) ->
            monthTo |> String.toInt |> Result.map (\m -> ( y, m, d ))
        )
        filters


handleDayChange : Filters -> String -> Msg
handleDayChange filters dayTo =
    handleDateChange
        (\( y, m, _ ) ->
            dayTo |> String.toInt |> Result.map (\d -> ( y, m, d ))
        )
        filters
