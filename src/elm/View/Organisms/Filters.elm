module View.Organisms.Filters exposing (view)

import Html exposing (Html, div, p, text, i, h2, h3, img, h1, button, label, input)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


--

import Model exposing (..)
import Styles exposing (styles)
import Msg exposing (Msg(..))


view : Filters -> Bool -> Html Msg
view filters isShown =
    let
        onChangeMode maybeMode _ =
            maybeMode
                |> Maybe.map List.singleton
                |> Maybe.withDefault allModes
                |> (\values -> ChangeFilter { filters | mode = values })

        onChangeStatus maybeStatus _ =
            maybeStatus
                |> Maybe.map List.singleton
                |> Maybe.withDefault allStatuses
                |> (\values -> ChangeFilter { filters | status = values })

        onChangePreference maybePreference _ =
            maybePreference
                |> Maybe.map List.singleton
                |> Maybe.withDefault allPreferences
                |> (\values -> ChangeFilter { filters | preference = values })

        toChoices allValues currentValues =
            [ Nothing => "All" => allValues == currentValues ]
                ++ (List.map (\v -> (Just v => toString v ++ " Only" => [ v ] == currentValues)) allValues)

        maybeHiddenClz =
            if isShown then
                ""
            else
                styles.hidden
    in
        div [ String.join " " [ "ui sixteen wide segment column", maybeHiddenClz ] |> class ]
            [ h2 [ class "ui medium dividing header" ] [ text "Filters" ]
            , div [ class "ui form" ]
                [ div [ class ("inline fields " ++ styles.filterItems) ]
                    [ checkboxFieldsView onChangeMode "Mode" (toChoices allModes filters.mode)
                    , checkboxFieldsView onChangeStatus "Status" (toChoices allStatuses filters.status)
                    , checkboxFieldsView onChangePreference "Preference" (toChoices allPreferences filters.preference)
                    , div [] []
                    ]
                ]
            ]


checkboxFieldsView toMsg title items =
    div [ class "ui grouped fields" ]
        ([ h3 [ class "ui small header" ] [ text title ] ] ++ (items |> List.map (\( v, ( l, b ) ) -> checkboxFieldView b (toMsg v) l)))


checkboxFieldView isChecked handleCheck labelText =
    div [ class "ui field" ]
        [ div [ class "ui radio checkbox" ]
            [ input [ type_ "radio", onCheck handleCheck, checked isChecked ] []
            , label [] [ text labelText ]
            ]
        ]
