module View.Organisms.ExecutionLogCard exposing (view)

import Date.Extra.Format as DateFormat
import Html exposing (Html, div, text, h4, a, img, span, button, p)
import Html.Attributes exposing (..)


--

import Model exposing (..)
import Utils.Dom as Dom
import Utils.GoogleMaps as GoogleMaps
import View.Atoms.Status


view : (Route -> msg) -> ExecutionLog -> Route -> Bool -> String -> Html msg
view toMsg record route isFocused gapiKey =
    let
        raised =
            if isFocused then
                "raised"
            else
                ""

        timeExpr =
            toFloat record.time / 60 |> toString

        modeExpr =
            case record.mode of
                Driving ->
                    "drive"

                Walking ->
                    "walk"
    in
        div [ class "column" ]
            [ div
                [ String.join " " [ "ui fluid", raised, "link card" ] |> class ]
                [ img [ GoogleMaps.panoramaUrl gapiKey record.origin.address |> src ] []
                , div [ class "content" ]
                    [ h4 [ class "ui small header" ]
                        [ status record |> View.Atoms.Status.view [ "right floated" ]
                        , div []
                            [ span [ class "sub header" ] [ timeExpr ++ " minutes' " ++ modeExpr ++ " from" |> text ]
                            , text record.origin.address
                            ]
                        ]
                    , p [] [ record.startDateTime |> DateFormat.isoString |> text ]
                    ]
                , Dom.a toMsg route [ class "ui bottom attached basic button" ] [ text "Focus" ]
                ]
            ]
