module View.Atoms.Marker exposing (view)

import Html exposing (Html, i)
import Html.Attributes exposing (..)


--

import Model exposing (..)
import Utils.GoogleMaps as GoogleMaps


view : ExecutionLog -> Float -> Html msg
view record size =
    i
        [ class "large map marker icon", style [ ( "color", GoogleMaps.markerColor record size ) ] ]
        []
