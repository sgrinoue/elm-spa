module View.Atoms.Status exposing (view)

import Html exposing (Html, div, text)
import Html.Attributes exposing (..)


--

import Model exposing (..)


view : List String -> Status -> Html msg
view clz status =
    let
        ( label, color ) =
            case status of
                Completed ->
                    ( "Done", "olive" )

                Started ->
                    ( "Started", "grey" )
    in
        div [ [ "ui label", color ] ++ clz |> String.join " " |> class ] [ label |> text ]
