module View.Molecules.Mask exposing (view)

import Html exposing (Html, div, p, text)
import Html.Attributes exposing (..)


--

import Styles exposing (styles)


view : Html msg
view =
    div [ class ("ui active inverted dimmer " ++ styles.mask) ]
        [ p [ class "ui text large loader" ] [ text "LOADING" ]
        ]
