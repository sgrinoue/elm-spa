port module Ports exposing (..)

import Json.Encode exposing (Value)


port initMap : Value -> Cmd msg


port setMapData : List Value -> Cmd msg


port initMapDone : (() -> msg) -> Sub msg


port featureClicked : (String -> msg) -> Sub msg


port detectDeviceType : List String -> Cmd msg


port detectDeviceTypeDone : (List (Maybe String) -> msg) -> Sub msg
