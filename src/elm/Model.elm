module Model exposing (..)

import Date exposing (Date)


type alias Flags =
    { apiHost : String
    , gapiKey : String
    , googleAnalyticsId : String
    }


type Status
    = Started
    | Completed


allStatuses : List Status
allStatuses =
    [ Started, Completed ]


type Mode
    = Walking
    | Driving


allModes : List Mode
allModes =
    [ Driving, Walking ]


type Preference
    = Balance
    | Precision
    | Speed


allPreferences : List Preference
allPreferences =
    [ Balance, Precision, Speed ]


type alias Id =
    String


type Route
    = Login LoginPageModel
    | ListView ListPageModel
    | Dashboard DashboardPageModel
    | NotFound (Maybe ( String, Route ))
    | MapView MapPageModel


type alias MapOptions =
    { center : LatLng
    , zoom : Int
    , gestureHandling : String
    }


type alias LatLng =
    { lat : Float, lng : Float }


type alias Origin =
    { address : String
    , latLng : LatLng
    }


type alias ExecutionLog =
    { id : String
    , startDateTime : Date
    , completeDateTime : Maybe Date
    , path : Maybe (List LatLng)
    , origin : Origin
    , mode : Mode
    , time : Int
    , clientIp : String
    , preference : Preference
    , avoidFerries : Bool
    , avoidHighways : Bool
    , avoidTolls : Bool
    , anglePerStep : Int
    , url : String
    , userAgent : Maybe String
    , viewport : Maybe BrowserViewport
    , encodedRequest : Maybe String
    , deviceType : Maybe String
    }


type alias BrowserViewport =
    { width : Int, height : Int }


type Level
    = Info
    | Error


type alias Notification =
    { level : Level, text : String }


type alias Account =
    { name : String, avatarImageUrl : String }


type alias Filters =
    { mode : List Mode
    , status : List Status
    , preference : List Preference
    , dateFrom : Date
    , dateTo : Date
    }


defaultFilters : Filters
defaultFilters =
    { mode = allModes
    , status = allStatuses
    , preference = allPreferences
    , dateFrom = Date.fromTime 0
    , dateTo = Date.fromTime 0
    }


type alias Model =
    { account : Maybe Account
    , isFilterViewShown : Bool
    , isHeaderMenuShown : Bool
    , isMenuShown : Bool
    , isTransitioning : Bool
    , notification : Maybe Notification
    , route : Route
    , today : Date
    , cache : Maybe Cache
    , flags : Flags
    }


type alias Cache =
    { filters : Filters
    , records : List ExecutionLog
    }


type alias MapPageModel =
    { focused : Maybe Id
    , filters : Filters
    , isMapLoading : Bool
    , mapOptions : MapOptions
    , records : List ExecutionLog
    }


type alias ListPageModel =
    { records : List ExecutionLog
    , filters : Filters
    }


type alias DashboardPageModel =
    { records : List ExecutionLog
    , filters : Filters
    }


type alias LoginPageModel =
    { name : String
    , password : String
    , error : Maybe String
    , topImageUrl : String
    , isRequesting : Bool
    }


(=>) : a -> b -> ( a, b )
(=>) a b =
    ( a, b )
infixr 0 =>


applyFilters : Filters -> List ExecutionLog -> List ExecutionLog
applyFilters filters records =
    let
        f : ExecutionLog -> Bool
        f record =
            List.member record.mode filters.mode
                && List.member (status record) filters.status
                && List.member record.preference filters.preference
    in
        List.filter f records


isFiltered : Filters -> Bool
isFiltered filters =
    (filters.mode /= allModes)
        || (filters.status /= allStatuses)
        || (filters.preference /= allPreferences)


status : ExecutionLog -> Status
status record =
    case record.completeDateTime of
        Nothing ->
            Started

        Just _ ->
            Completed


initialModel : Model
initialModel =
    { account = Nothing
    , isFilterViewShown = False
    , isHeaderMenuShown = False
    , isMenuShown = True
    , isTransitioning = False
    , notification = Nothing
    , route = NotFound Nothing
    , today = Date.fromTime 0
    , cache = Nothing
    , flags =
        { apiHost = ""
        , gapiKey = ""
        , googleAnalyticsId = ""
        }
    }


initialLoginPageModel : LoginPageModel
initialLoginPageModel =
    { name = ""
    , password = ""
    , error = Nothing
    , topImageUrl = "https://1.bp.blogspot.com/-AmMTU5Pn5kA/UYurZbCOmMI/AAAAAAAARzY/m7aOG8IBifw/s400/kouyou_family.png"
    , isRequesting = False
    }


initialMapPageModel : MapPageModel
initialMapPageModel =
    { focused = Nothing
    , filters = defaultFilters
    , isMapLoading = False
    , mapOptions =
        { zoom = 8
        , center = { lat = 35, lng = 138 }
        , gestureHandling = "greedy"
        }
    , records = []
    }


initialDashboardPageModel : DashboardPageModel
initialDashboardPageModel =
    { records = []
    , filters = defaultFilters
    }


initialListPageModel : ListPageModel
initialListPageModel =
    { records = []
    , filters = defaultFilters
    }
