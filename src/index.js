import MobileDetect from 'mobile-detect';
import './style.css';
import * as map from './map';
import Elm from './elm/Main.elm';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.createElement('script');
  const key = process.env.GOOGLE_API_KEY;

  el.src = `https://maps.googleapis.com/maps/api/js?key=${key}&callback=initApp`;

  document.head.appendChild(el);
});

window.initApp = () => {
  const retryInterval = 100;
  const google = global.google;
  const app = Elm.Main.embed(document.getElementById('app'), {
    apiHost: process.env.API_HOST,
    gapiKey: process.env.GOOGLE_API_KEY,
    googleAnalyticsId: process.env.GOOGLE_ANALYTICS_ID,
  });

  app.ports.initMap.subscribe(initMap);
  app.ports.setMapData.subscribe(setMapData);
  app.ports.detectDeviceType.subscribe(detectDeviceType);

  if ('serviceWorker' in window.navigator) {
    navigator.serviceWorker.register('/service-worker.js');
  }

  let mapCache = null;

  function initMap(mapOptions) {
    const el = document.getElementById('map');

    if (el === null) {
      setTimeout(() => initMap(mapOptions), retryInterval);
    } else {
      const cached = el && el.children.length > 0 && mapCache;

      if (cached) {
        map.applyMapOptions(google, cached, mapOptions);
      } else {
        mapCache = map.initMapImpl(google, el, mapOptions, id =>
          app.ports.featureClicked.send(id),
        );
      }
      app.ports.initMapDone.send(null);
    }
  }

  function setMapData(data) {
    const el = document.getElementById('map');

    if (mapCache === null || !el || el.children.length === 0) {
      setTimeout(() => setMapData(data), retryInterval);
    } else {
      map.setMapDataImpl(google, mapCache, data);
    }
  }

  function detectDeviceType(userAgents) {
    app.ports.detectDeviceTypeDone.send(
      userAgents.map(ua => {
        const result = new MobileDetect(ua);

        if (result.phone()) return 'phone';
        if (result.tablet()) return 'tablet';
        return 'pc';
      }),
    );
  }
};
