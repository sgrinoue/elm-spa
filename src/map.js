export function initMapImpl (google, el, mapOptions, onClickFeature) {
  const map = new google.maps.Map(el, mapOptions)

  map.data.setStyle(f => f.getProperty('style'))
  google.maps.event.addListener(map.data, 'click', ev => {
    const id = ev.feature.getId()

    if (!id) return

    onClickFeature(id)
  })

  return map
}

export function applyMapOptions (google, map, mapOptions) {
  if (map.getZoom() !== mapOptions.zoom) map.setZoom(mapOptions.zoom)
  if (!map.getCenter().equals(new google.maps.LatLng(mapOptions.center))) { map.panTo(mapOptions.center) }
}

export function setMapDataImpl (google, map, data) {
  const idsToSet = data.map(d => d.id)
  let featuresToRemove = []
  let dataToAdd = data

  map.data.forEach(f => {
    const id = f.getId()

    if (idsToSet.indexOf(id) < 0) featuresToRemove.push(f)
    dataToAdd = dataToAdd.filter(data => data.id !== id)
  })

  featuresToRemove.forEach(f => map.data.remove(f))
  map.data.addGeoJson({
    type: 'FeatureCollection',
    features: dataToAdd
  })
}
