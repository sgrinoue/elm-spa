variable "gcp_project_id" {
  type = "string"
}

variable "backend_deploy_bucket_name" {
  type = "string"
}

provider "google" {
  project = "${var.gcp_project_id}"
}

data archive_file "list_execution_logs_function_source" {
  type        = "zip"
  source_dir  = "./backend/src/list-execution-logs"
  output_path = "./backend/target/list-execution-logs.zip"
}

resource google_storage_bucket "backend_deploy_bucket" {
  name          = "${var.backend_deploy_bucket_name}"
  location      = "us-central1"
  storage_class = "REGIONAL"
}

resource google_storage_bucket_object "list_execution_logs_function_assets" {
  name   = "list_execution_logs_function_assets.zip"
  source = "${data.archive_file.list_execution_logs_function_source.output_path}"
  bucket = "${google_storage_bucket.backend_deploy_bucket.name}"
}

