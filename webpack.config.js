const CopyPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const ElmPostCss = require('postcss-elm-modules');
const path = require('path');
const webpack = require('webpack');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: {
          loader: 'elm-webpack-loader',
          options: {
            warn: true,
            verbose: true,
          },
        },
      },
      {
        test: /\.css$/,
        exclude: [/node_modules/],
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[hash:base64]',
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  ElmPostCss({
                    cssModules: {
                      enabled: true,
                      scopePattern: '[hash:base64]',
                    },
                    moduleName: 'styles',
                    dir: './generated',
                  }),
                ],
              },
            },
          ],
        }),
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.API_HOST': JSON.stringify(process.env.API_HOST),
      'process.env.GOOGLE_API_KEY': JSON.stringify(process.env.GOOGLE_API_KEY),
      'process.env.GOOGLE_ANALYTICS_ID': JSON.stringify(
        process.env.GOOGLE_ANALYTICS_ID,
      ),
    }),
    new webpack.HotModuleReplacementPlugin(),
    new CopyPlugin([
      {
        from: './index.html',
        to: path.resolve(__dirname, 'dist/index.html'),
      },
    ]),
    new ExtractTextPlugin('style.css'),
    /*
    new SWPrecacheWebpackPlugin({
      cacheId: 'elm-console-cache',
      filename: 'service-worker.js',
      navigateFallback: '/index.html',
    }),
    */
  ],
  devServer: {
    hot: true,
    host: process.env.DEV_SERVER_HOST || 'localhost',
    historyApiFallback: {
      index: 'index.html',
    },
  },
};
